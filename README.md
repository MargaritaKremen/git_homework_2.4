Завдання 4

Надішліть репозиторій на віддалений сервер.

Завдання 5

Додайте ще гілки dev та test на віддаленому репозиторії й додайте у кожну по 5 файлів. Виконайте коміти файлів. Відобразіть усі гілки на екрані та усі коміти у гілках.

Завдання 6

Виконайте клонування репозиторія з віддаленного серверу на свій комп’ютер.
